import { HttpService } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { NewsService } from '../../news/news.service';
import { ApiService } from '../api.service';
import { hitsStub } from './stubs/hits.stub';
import { of } from 'rxjs';

describe('ApiService', () => {
  let apiService: ApiService;
  let httpService: HttpService;
  let newsService: NewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ApiService,
        {
          provide: HttpService,
          useValue: {
            get: jest.fn(() =>
              of({
                data: { hits: hitsStub() },
              }),
            ),
          },
        },
        {
          provide: NewsService,
          useValue: {
            getNew: jest.fn(),
            createNew: jest.fn(),
          },
        },
      ],
    }).compile();

    apiService = module.get<ApiService>(ApiService);
    httpService = module.get<HttpService>(HttpService);
    newsService = module.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(apiService).toBeDefined();
  });

  it('should be defined', () => {
    expect(httpService).toBeDefined();
  });

  it('should be defined', () => {
    expect(newsService).toBeDefined();
  });

  describe('getApiData', () => {
    it('should return all hits', async () => {
      jest.spyOn(httpService, 'get');
      const hits = await apiService.getApiData();
      expect(JSON.stringify(hits)).toStrictEqual(JSON.stringify(hitsStub()));
    });
  });

  describe('handleApiData', () => {
    describe('when handleApiData is called', () => {
      beforeEach(async () => {
        await apiService.handleApiData();
      });

      it('getApiData must be called', () => {
        expect(newsService.getNew).toHaveBeenCalled();
      });

      it('getApiData must be called', () => {
        expect(newsService.createNew).toHaveBeenCalled();
      });
    });
  });
});
