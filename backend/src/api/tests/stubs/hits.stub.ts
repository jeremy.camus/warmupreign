export const hitsStub = () => [
  {
    objectID: 12345,
    story_title: 'story_title',
    story_url: 'https://story_url.com/',
    title: 'title',
    url: 'https://url.com/',
    author: 'author',
    created_at: new Date('21-07-22, 04:59:15'),
  },
];
