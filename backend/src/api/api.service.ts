import { HttpService } from '@nestjs/axios';
import { Injectable, OnModuleInit, Logger } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { map } from 'rxjs';
import { firstValueFrom } from 'rxjs';
import { createNewsDto } from '../news/dto/create_new.dto';
import { NewsService } from '../news/news.service';

@Injectable()
export class ApiService implements OnModuleInit {
  constructor(
    private readonly httpService: HttpService,
    private readonly newsService: NewsService,
  ) {}

  async onModuleInit() {
    await this.handleApiData();
  }

  async getApiData(): Promise<Array<object>> {
    const res = this.httpService
      .get(process.env.API_URL)
      .pipe(map((response) => response.data.hits));
    return await firstValueFrom(res);
  }

  @Cron(CronExpression.EVERY_HOUR)
  async handleApiData() {
    Logger.log('FETCHING TO HN API');
    const news: Array<object> = await this.getApiData();
    news.map(async (data: createNewsDto) => {
      const newInstance = await this.newsService.getNew(data.objectID);
      if (!newInstance) await this.newsService.createNew(data);
    });
  }
}
