import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';
import { NewsModule } from '../news/news.module';
import { ApiService } from './api.service';

@Module({
  imports: [HttpModule, NewsModule, ScheduleModule.forRoot()],
  providers: [ApiService],
})
export class ApiModule {}
