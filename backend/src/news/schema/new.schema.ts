import { Schema } from 'mongoose';

export const NewSchema = new Schema({
  objectID: Number,
  story_title: String,
  story_url: String,
  title: String,
  url: String,
  author: String,
  created_at: Date,
  status: {
    type: Boolean,
    default: true,
  },
  deleted_at: {
    type: Date,
    default: null,
  },
});

NewSchema.methods.toJSON = function () {
  const { __v, _id, ...newData } = this.toObject();
  newData.id = _id;
  return newData;
};
