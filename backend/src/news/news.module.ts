import { Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { MongooseModule } from '@nestjs/mongoose';
import { NewSchema } from './schema/new.schema';
import { NewRepository } from './repository/new.repository';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'New', schema: NewSchema }])],
  controllers: [NewsController],
  providers: [NewsService, NewRepository],
  exports: [NewsService],
})
export class NewsModule {}
