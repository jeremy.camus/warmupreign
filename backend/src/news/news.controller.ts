import { Controller, Delete, Get, Param } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { NewDocument } from './interfaces/new.document';
import { NewsService } from './news.service';

@Controller('news')
export class NewsController {
  constructor(private newService: NewsService) {}

  @ApiTags('News')
  @Get()
  getAllNews(): Promise<Array<NewDocument>> {
    return this.newService.getNews();
  }

  @ApiTags('News')
  @Delete(':objectID')
  deleteNewById(@Param('objectID') objectID: string): Promise<NewDocument> {
    return this.newService.deleteNew(Number(objectID));
  }
}
