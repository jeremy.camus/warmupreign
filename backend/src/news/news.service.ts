import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { createNewsDto } from './dto/create_new.dto';
import { NewDocument } from './interfaces/new.document';
import { NewRepository } from './repository/new.repository';

@Injectable()
export class NewsService {
  constructor(private readonly newRepository: NewRepository) {}

  private readonly httpException = new HttpException(
    {
      status: HttpStatus.INTERNAL_SERVER_ERROR,
      error: 'Database conection error',
    },
    HttpStatus.INTERNAL_SERVER_ERROR,
  );

  async getNews(): Promise<Array<NewDocument>> {
    try {
      return await this.newRepository.getAll();
    } catch (error) {
      Logger.error(error);
      throw this.httpException;
    }
  }

  async getNew(objectID: number): Promise<NewDocument> {
    try {
      return await this.newRepository.getOne({ objectID });
    } catch (error) {
      Logger.error(error);
      throw this.httpException;
    }
  }

  async createNew(newData: createNewsDto): Promise<NewDocument> {
    try {
      return await this.newRepository.create(newData);
    } catch (error) {
      Logger.error(error);
      throw this.httpException;
    }
  }

  async deleteNew(objectID: number): Promise<NewDocument> {
    try {
      return await this.newRepository.delete({ objectID });
    } catch (error) {
      Logger.error(error);
      throw this.httpException;
    }
  }
}
