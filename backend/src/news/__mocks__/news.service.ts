import { newStub } from '../tests/stubs/new.stub';

export const NewsService = jest.fn().mockReturnValue({
  getNews: jest.fn().mockResolvedValue([newStub()]),
  deleteNew: jest.fn().mockResolvedValue({ ...newStub(), status: false }),
});
