import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { createNewsDto } from '../dto/create_new.dto';
import { NewDocument } from '../interfaces/new.document';

@Injectable()
export class NewRepository {
  constructor(
    @InjectModel('New') private readonly newModel: Model<NewDocument>,
  ) {}

  getAll(): Promise<Array<NewDocument>> {
    return this.newModel.find().exec();
  }

  getOne(filters: object): Promise<NewDocument> {
    return this.newModel.findOne(filters).exec();
  }

  create(newData: createNewsDto): Promise<NewDocument> {
    return this.newModel.create(newData);
  }

  delete(filters: object): Promise<NewDocument> {
    return this.newModel
      .findOneAndUpdate(
        filters,
        { status: false, deleted_at: new Date() },
        { new: true },
      )
      .exec();
  }
}
