import { HttpException, HttpStatus } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { NewsService } from '../news.service';
import { NewRepository } from '../repository/new.repository';
import { newDocumentStub } from './stubs/new.document.stub';
import { newStub } from './stubs/new.stub';

describe('NewsService', () => {
  let newsService: NewsService;
  let newRepository: NewRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        {
          provide: NewRepository,
          useValue: {
            getAll: jest.fn(),
            getOne: jest.fn(),
            create: jest.fn((data) => ({ id: 1, ...data, deleted_at: null })),
            delete: jest.fn(),
          },
        },
      ],
    }).compile();

    newsService = module.get<NewsService>(NewsService);
    newRepository = module.get<NewRepository>(NewRepository);
    jest.clearAllMocks();
  });

  type serviceMethodType = 'create' | 'delete' | 'getAll' | 'getOne';

  const httpErrorTest = (serviceMethodString: serviceMethodType) => {
    jest.spyOn(newRepository, serviceMethodString).mockImplementation(() => {
      throw new HttpException(
        {
          status: HttpStatus.INTERNAL_SERVER_ERROR,
          error: 'Database conection error',
        },
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    });
  };

  it('should be defined', () => {
    expect(newsService).toBeDefined();
  });

  describe('getNews', () => {
    it('should return all news', async () => {
      jest
        .spyOn(newRepository, 'getAll')
        .mockReturnValue([newDocumentStub()] as any);
      const news = await newsService.getNews();
      expect(JSON.stringify(news)).toStrictEqual(JSON.stringify([newStub()]));
    });
  });

  describe('getNewsDbError', () => {
    it('should return a internal server error', async () => {
      httpErrorTest('getAll');
      try {
        await newsService.getNews();
      } catch (error) {
        expect(error.response).toStrictEqual({
          status: 500,
          error: 'Database conection error',
        });
      }
    });
  });

  describe('getNew with the objectID', () => {
    it('should return a new', async () => {
      jest
        .spyOn(newRepository, 'getOne')
        .mockReturnValueOnce(newDocumentStub() as any);
      const newInstance = await newsService.getNew(12345);
      expect(JSON.stringify(newInstance)).toStrictEqual(
        JSON.stringify(newStub()),
      );
    });
  });

  describe('getNewDbError', () => {
    it('should return a internal server error', async () => {
      httpErrorTest('getOne');
      try {
        await newsService.getNew(12345);
      } catch (error) {
        expect(error.response).toStrictEqual({
          status: 500,
          error: 'Database conection error',
        });
      }
    });
  });

  describe('createNew', () => {
    it('should return a new instance', async () => {
      const newInstance = await newsService.createNew({
        objectID: 12345,
        story_title: 'story_title',
        story_url: 'https://story_url.com/',
        title: 'title',
        url: 'https://url.com/',
        author: 'author',
        created_at: new Date('21-07-22, 04:59:15'),
        status: true,
      });
      expect(JSON.stringify(newInstance)).toStrictEqual(
        JSON.stringify(newStub()),
      );
    });
  });

  describe('createNewDbError', () => {
    it('should return a internal server error', async () => {
      httpErrorTest('create');
      try {
        await newsService.createNew({
          objectID: 12345,
          story_title: 'story_title',
          story_url: 'https://story_url.com/',
          title: 'title',
          url: 'https://url.com/',
          author: 'author',
          created_at: new Date('21-07-22, 04:59:15'),
          status: true,
        });
      } catch (error) {
        expect(error.response).toStrictEqual({
          status: 500,
          error: 'Database conection error',
        });
      }
    });
  });

  describe('deleteNew', () => {
    it('should update and return the new instance updated', async () => {
      jest.spyOn(newRepository, 'delete').mockReturnValueOnce({
        ...newDocumentStub(),
        status: false,
        deleted_at: new Date().toISOString().split('T')[0],
      } as any);
      const newInstance = await newsService.deleteNew(12345);
      expect(JSON.stringify(newInstance)).toStrictEqual(
        JSON.stringify({
          ...newStub(),
          status: false,
          deleted_at: new Date().toISOString().split('T')[0],
        }),
      );
    });
  });

  describe('deleteNewDbError', () => {
    it('should return a internal server error', async () => {
      httpErrorTest('delete');
      try {
        await newsService.deleteNew(12345);
      } catch (error) {
        expect(error.response).toStrictEqual({
          status: 500,
          error: 'Database conection error',
        });
      }
    });
  });
});
