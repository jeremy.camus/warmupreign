import { Test, TestingModule } from '@nestjs/testing';
import { New } from '../interfaces/new';
import { NewsController } from '../news.controller';
import { NewsService } from '../news.service';
import { newStub } from './stubs/new.stub';

jest.mock('../news.service');

describe('NewsController', () => {
  let newsController: NewsController;
  let newsService: NewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [NewsService],
    }).compile();

    newsController = module.get<NewsController>(NewsController);
    newsService = module.get<NewsService>(NewsService);
    jest.clearAllMocks();
  });

  describe('getNews', () => {
    describe('When getNews is call', () => {
      let news: Array<New>;
      const expectedNews: Array<New> = [newStub()];
      beforeEach(async () => {
        news = await newsController.getAllNews();
      });

      test('Then it should call newsService', () => {
        expect(newsService.getNews).toHaveBeenCalled();
      });

      test('Then it should return news', () => {
        expect(JSON.stringify(news)).toStrictEqual(
          JSON.stringify(expectedNews),
        );
      });
    });
  });

  describe('deleteNews', () => {
    describe('When deleteNews is call', () => {
      let newInstance: New;
      const expectedNew: New = { ...newStub(), status: false };
      beforeEach(async () => {
        newInstance = await newsController.deleteNewById('12345');
      });

      test('Then it should call newsService', () => {
        expect(newsService.deleteNew).toHaveBeenCalled();
      });

      test('Then it should return the updated new', () => {
        expect(JSON.stringify(newInstance)).toStrictEqual(
          JSON.stringify(expectedNew),
        );
      });
    });
  });
});
