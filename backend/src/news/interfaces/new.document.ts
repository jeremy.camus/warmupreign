import { Document } from 'mongoose';

export interface NewDocument extends Document {
  id?: number;
  objectID: number;
  story_title: string;
  story_url: string;
  title: string;
  url: string;
  author: string;
  created_at: Date;
  status: boolean;
  deleted_at: Date;
}
