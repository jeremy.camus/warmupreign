# WarmUp Backend

## Features

### Schemas

NewsSchema:

- objectID: Number
- story_title: String
- story_url: String
- title: String
- url: String
- author: String
- created_at: Date
- status: Boolean

### Endpoints

1. Get News endpoint: In this endpoint all the news that are in the database are obtained.
2. Get New endpoint: In this endpoint a specific news is obtained for this the objectID of the new must be passed by parameters.
3. Create New endpoint: In this endpoint It this endpoint, a new instance of a news item is created in the database.
4. Delete New endpoint: In this endpoint the status of a news is changed to false for this the objectID of the new must be passed by parameters.
5. Get Api Data endpoint: In this endpoint a get request is made to the hackers news api obtaining all the data that it returns.
6. Handle Api Data endpoint: In this endpoint all the data obtained in the previous endpoint is handled, it is validated that each new news obtained is created in the database. This endpoint is scheduled to be called every hour

### Populate of the data base

To populate the database it has been programmed that every time the server starts it executes the HandleApiData service, in this way the database is populated with the news from the Hacker News API. To achieve this the OnModuleInit interface is used.

### Tests

Unit tests have been written for controllers and services and e2e tests for the newsModule module.

To carry out the unit tests, mocks and stubs have been used depending on what want to test. And for the e2e tests, a mongodb database is created in memory, in this way you can better control the data that the database has to test it

#### How to run

To see the unit tests

```bash
npm run test
```

To see the unit tests coverage

```bash
npm run test:cov
```

To see the e2e tests

```bash
npm run test:e2e
```

#### Coverage

Here is evidence of the tests coverages:

![Coverage](https://i.ibb.co/1ZwBDDP/Captura-de-pantalla-de-2022-08-04-11-01-34.png)

The logs of errors is for the httpException tests, this because the service function logs the error.
