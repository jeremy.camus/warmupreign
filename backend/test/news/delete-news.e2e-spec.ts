import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { NewsModule } from '../../src/news/news.module';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { connect, Connection, Model } from 'mongoose';
import { NewDocument } from '../../src/news/interfaces/new.document';
import { NewSchema } from '../../src/news/schema/new.schema';
import { getModelToken, MongooseModule } from '@nestjs/mongoose';

describe('NewsController (e2e)', () => {
  let app: INestApplication;
  let mongodb: MongoMemoryServer;
  let mongoConnection: Connection;
  let newsModel: Model<NewDocument>;

  const dateString = '2022-07-27T20:29:52.000Z';
  const newMockData = {
    objectID: 12345,
    story_title: 'story_title',
    story_url: 'https://story_url.com/',
    title: 'title',
    url: 'https://url.com/',
    author: 'author',
    created_at: new Date(dateString),
  };

  beforeEach(async () => {
    mongodb = await MongoMemoryServer.create();
    const uri = mongodb.getUri();
    mongoConnection = (await connect(uri)).connection;
    newsModel = mongoConnection.model('New', NewSchema);

    const moduleFixture: TestingModule = await Test.createTestingModule({
      providers: [{ provide: getModelToken('New'), useValue: newsModel }],
      imports: [NewsModule, MongooseModule.forRoot(uri)],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();

    const newMock = new newsModel(newMockData);
    await newMock.save();
  });

  afterAll(async () => {
    await mongoConnection.dropDatabase();
    await mongoConnection.close();
    await mongodb.stop();
    await app.close();
  });

  describe('NEWS CONTROLLER', () => {
    it('/news (DELETE)', async () => {
      const newMockInstance = await newsModel
        .findOne({
          objectID: newMockData.objectID,
        })
        .exec();

      const expectedData = {
        ...newMockData,
        created_at: dateString,
        status: false,
        deleted_at: new Date().toISOString().split('T')[0],
        id: newMockInstance.id.toString(),
      };

      const res = await request(app.getHttpServer())
        .delete(`/news/${newMockData.objectID}`)
        .expect(200);

      const deleted_at = new Date(res.body.deleted_at);

      expect({
        ...res.body,
        deleted_at: deleted_at.toISOString().split('T')[0],
      }).toStrictEqual(expectedData);
    });
  });
});
