# WarmUp Reign School Project

## Resume

The server takes care of pulling the data from Hacker News api into mongoDB and expose an API for the client. The client should render a web page that lists the news in chronological order.

## Workflow

To carry out the project, git flow was used, for a better experience with this workflow, the hub flow command line was used.

## Features

### Backend

The server makes a request to the Hacker News api every hour, the received data is entered into a mongoDB database, and it has two endpoints to be consumed by the client.

#### Endpoints

1. Get News endpoint: In this endpoint all the news that are in the database are obtained.
2. Get New endpoint: In this endpoint a specific news is obtained for this the objectID of the new must be passed by parameters.
3. Create New endpoint: In this endpoint It this endpoint, a new instance of a news item is created in the database.
4. Delete New endpoint: In this endpoint the status of a news is changed to false for this the objectID of the new must be passed by parameters.
5. Get Api Data endpoint: In this endpoint a get request is made to the hackers news api obtaining all the data that it returns.
6. Handle Api Data endpoint: In this endpoint all the data obtained in the previous endpoint is handled, it is validated that each new news obtained is created in the database.

**The client only consumes the Get News and Remove New endpoints.**

### Frontend

A web page is rendered showing the latest news obtained from the server in chronological order. Also, each news has a delete button, which removes the news from the list and does not appear again.

#### Features

1. Banner: Here shows the title "HN Feed" and the subtitle "We <3 hacker news".
2. List of news: Here all the news available from the server are shown, these are filtered showing only those that have a title and link available, this so that when clicking on a news item it is redirected to the news item itself, also, each news item has a button to delete the respective news, which will not appear again.

## How to run

### As production

To run as production you only have to run the docker compose command

```bash
docker compose up
```

After executing this command the docker container will have been created, the ports of the respective servers are

For the frontend `localhost:3000`

For the backend `localhost:8080`

### As development

First we have to install de node dependencies.

For the backend:

```bash
cd backend
npm i
cd ..
```

For the frontend:

```bash
cd frontend
npm i
cd ..
```

Now we can execute the docker compose command

```bash
docker compose -f docker-compose-dev.yml up
```

After executing this command the docker container will have been created, the ports of the respective servers are

For the frontend `localhost:3000`

For the backend `localhost:8080`
