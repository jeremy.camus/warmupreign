# WarmUp Frontend

This is the frontend of the Reign School WarmUp project.

In summary, here all the news coming from the backend are listed, it should be noted that only the news that have a title or story title, url or sotry url are shown and cannot have the status of deleted.

To build an easy to modify user interface, a theme has been created which is distributed in the app with a provider, to achieve this the styled-components library was used, this also, to create components directly with static or customizable styles

## Page Screenshot

![Page Image](https://i.ibb.co/K7hXkPV/Captura-de-pantalla-de-2022-07-26-16-45-44.png)

## Features

### Banner

Only the requested title which is "HN Feed" and the subtitle which is "we <3 hackernews" are displayed here.

To implement the banner, a specific component was created for it, composed of a primary and secondary title component.

### List of news

Here all the news coming from the backend are listed, only the news that have a title or story title, url or sotry url are shown.

To implement this list, a component was created for the list, which goes through the news obtained from the server, this data is passed to a specific component of each row, which is divided into different abstraction layers, this to decouple the complexity, deep down you find that the row is composed of three Text components, showing the title, author and date of the news, and a button component which is assigned the behavior to delete a news.
