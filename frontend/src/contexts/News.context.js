import { createContext, useState } from 'react';

export const NewsContext = createContext({ news: [], setNews: () => [] });

export const NewsProvider = ({ children }) => {
  const [news, setNews] = useState([]);
  const ctx = {
    news,
    setNews: (data) => setNews(data),
  };

  return <NewsContext.Provider value={ctx}>{children}</NewsContext.Provider>;
};
