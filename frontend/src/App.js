import Banner  from './components/Banner/';
import List from './components/List';
import { NewsProvider } from './contexts/News.context';

const App = () => {
  return (
    <NewsProvider>
      <Banner />
      <List />
    </NewsProvider>
  );
};

export default App;
