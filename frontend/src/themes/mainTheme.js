export const mainTheme = {
  text: '#333',
  authorText: '#999',
  fontSize: '13pt',
  rowBg: '#fff',
  rowBgOver: '#fafafa',
  rowBorder: '#ccc',
  buttonHover: '#eb443a',
  bannerBg: '#303030',
  bannerText: '#fff',
};
