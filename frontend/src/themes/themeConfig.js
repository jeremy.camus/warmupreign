import { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  * {
    box-sizing: border-box;
    scroll-behavior: smooth;
    font-family: 'Montserrat', -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Oxygen, Ubuntu, Cantarell, 'Open Sans', 'Helvetica Neue', sans-serif;
  }
  
  html, body {
    margin: 0;
    padding: 0;
  }

  body {
    color: ${({ theme }) => theme.text};
    height: 100%;
  }

  p {
    font-size: ${({ theme }) => theme.fontSize}
  }

  a {
    text-decoration: none;
    color: inherit;
  }

  li {
    list-style-type: none;
    background-color: ${({ theme }) => theme.rowBg};
    border-bottom: 1px solid ${({ theme }) => theme.rowBorder};
  }

  li:hover {
    background-color: ${({ theme }) => theme.rowBgOver};
  }
`;

export default GlobalStyle;
