import StyledSecondaryTitle from './styles/SecondaryTitle.styled';

export const SecondaryTitle = ({ children }) => {
  return <StyledSecondaryTitle>{children}</StyledSecondaryTitle>;
};

export default SecondaryTitle;
