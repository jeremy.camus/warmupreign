import styled from 'styled-components';
const StyledSecondaryTitle = styled.h2`
  margin: 0;
  font-size: 20pt;
`;

export default StyledSecondaryTitle;
