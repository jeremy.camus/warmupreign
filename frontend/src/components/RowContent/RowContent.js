import RowContainer from './containers/Row.container';
import Button from '../Button';
import RowSection from './RowSection';
import Text from '../Text';
import { FaTrashAlt } from 'react-icons/fa';
import Link from '../Link';
import { useHandleDeleteNew } from '../../hooks/useHandleNews';
import { useContext } from 'react';
import { NewsContext } from '../../contexts/News.context';

const successsCode = 200;

const RowContent = ({ data }) => {
  const { news, setNews } = useContext(NewsContext);
  const handleDeleteNew = useHandleDeleteNew;

  const handleOnClick = async () => {
    const responseStatus = await handleDeleteNew(data.objectID);
    if (responseStatus !== successsCode) {
      return;
    }
    const newsFiltered = news.filter((element) => {
      return element.objectID !== data.objectID;
    });
    setNews(newsFiltered);
  };

  return (
    <RowContainer>
      <Link data={data}>
        <RowSection width={'84%'}>
          <Text>{data.title ? data.title : data.story_title}</Text>
          <Text isAuthor={true}> - {data.author} - </Text>
        </RowSection>
      </Link>
      <RowSection width={'15%'} justify={'space-between'}>
        <Text>{data.created_at}</Text>
        <Button data={data} fontSize={'15pt'} onClick={handleOnClick}>
          <FaTrashAlt />
        </Button>
      </RowSection>
    </RowContainer>
  );
};

export default RowContent;
