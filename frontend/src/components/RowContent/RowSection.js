import StyledRowSection from './styles/RowSection.styled';

const RowSection = ({ children, width, justify }) => {
  return (
    <StyledRowSection width={width} justify={justify}>
      {children}
    </StyledRowSection>
  );
};

export default RowSection;
