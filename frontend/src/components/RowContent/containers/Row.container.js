import styled from 'styled-components';

const RowContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: row;
  margin: 0 20px;
  width: 100%;
`;

export default RowContainer;
