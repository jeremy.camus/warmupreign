import styled from 'styled-components';

const StyledRowSection = styled.div`
  display: flex;
  justify-content: ${(props) => props.justify ? props.justify: 'flex_start'};
  width: ${(props) => props.width ? props.width: 0};
  margin: 20px 0;
`;

export default StyledRowSection;
