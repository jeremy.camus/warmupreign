import StyledBanner from './styles/Banner.styled';
import PrimaryTitle from '../PrimaryTitle';
import SecondaryTitle from '../SecondaryTitle';

const Banner = () => {
  return (
    <StyledBanner>
      <div>
        <PrimaryTitle>HN Feed</PrimaryTitle>
        <SecondaryTitle>We {'<3'} Hacker News!</SecondaryTitle>
      </div>
    </StyledBanner>
  );
};

export default Banner;
