import styled from 'styled-components';

const StyledBanner = styled.div`
  display: flex;
  align-items: center;
  height: 25vh;
  padding: 0 50px;
  background-color: ${(props) => props.theme.bannerBg};
  color: ${(props) => props.theme.bannerText};
`;

export default StyledBanner;
