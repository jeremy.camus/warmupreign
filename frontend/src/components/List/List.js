import { useContext, useEffect } from 'react';
import { NewsContext } from '../../contexts/News.context';
import { useHandleGetNews } from '../../hooks/useHandleNews';
import StyledList from './styles/List.styled';
import Row from '../Row';

const List = () => {
  const { news, setNews } = useContext(NewsContext);
  const handleGetNews = useHandleGetNews;

  useEffect(() => {
    const fetchData = async () => {
      const data = await handleGetNews();
      setNews(data);
    };
    fetchData();
  }, []);

  const isValidNew = (data) => {
    return (
      data.status &&
      (data.story_title || data.title) &&
      (data.story_url || data.url)
    );
  };

  return (
    <StyledList>
      {news.map((data) =>
        isValidNew(data) ? <Row key={data.objectID} data={data} /> : null,
      )}
    </StyledList>
  );
};

export default List;
