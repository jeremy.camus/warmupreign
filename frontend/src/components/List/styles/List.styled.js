import styled from 'styled-components';

const StyledList = styled.ul`
  margin: 0 50px;
`;

export default StyledList;
