import styled from 'styled-components';

const StyledRow = styled.li`
  display: flex;
  flex-direction: row;
  align-items: center;

  & > div > div > button {
    visibility: hidden;
  }

  &:hover {
    & > div > div > button {
      visibility: visible;
    }
  }
`;

export default StyledRow;
