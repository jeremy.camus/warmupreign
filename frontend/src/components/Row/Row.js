import StyledRow from './styles/Row.styled';
import RowContent from '../RowContent';

const Row = ({ data }) => {
  return (
    <StyledRow>
      <RowContent data={data} />
    </StyledRow>
  );
};

export default Row;
