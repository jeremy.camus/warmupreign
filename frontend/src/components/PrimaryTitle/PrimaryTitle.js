import StyledPrimaryTitle from './styles/PrimaryTitle.styled';

export const PrimaryTitle = ({ children }) => {
  return <StyledPrimaryTitle>{children}</StyledPrimaryTitle>;
};

export default PrimaryTitle;
