import styled from 'styled-components';
const StyledPrimaryTitle = styled.h1`
  margin: 0;
  font-size: 60pt;
`;

export default StyledPrimaryTitle;
