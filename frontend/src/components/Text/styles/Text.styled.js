import styled from 'styled-components';

const StyledText = styled.p`
  padding: 0 10px;
  color: ${(props) => (props.isAuthor ? props.theme.authorText : 'inherit')};
`;

export default StyledText;
