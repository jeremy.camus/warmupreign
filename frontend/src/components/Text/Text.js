import StyledText from './styles/Text.styled';

const Text = ({ children, isAuthor = false }) => {
  return <StyledText isAuthor={isAuthor}>{children}</StyledText>;
};

export default Text;
