import StyledLink from './styles/Link.styled';

const Link = ({ children, data }) => {
  return (
    <StyledLink href={data.url ? data.url : data.story_url} target={'_blanck'}>
      {children}
    </StyledLink>
  );
};

export default Link;
