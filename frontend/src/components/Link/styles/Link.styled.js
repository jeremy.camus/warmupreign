import styled from 'styled-components';

const StyledLink = styled.a`
  width: 100%;
  text-decoration: none;
  color: inherit;
`;

export default StyledLink;
