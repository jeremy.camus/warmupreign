import styled from 'styled-components';

const StyledButton = styled.button`
  background: none;
  border: none;
  padding: 0;
  cursor: pointer;
  font-size: ${(props) => (props.fontSize ? props.fontSize : 'inherit')};

  &:hover {
    color: ${(props) => props.theme.buttonHover};
  }
`;

export default StyledButton;
