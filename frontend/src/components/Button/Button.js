import StyledButton from './styles/Button.styled';

const Button = ({ children, fontSize, onClick }) => {
  return (
    <StyledButton onClick={() => onClick()} fontSize={fontSize}>
      {children}
    </StyledButton>
  );
};

export default Button;
