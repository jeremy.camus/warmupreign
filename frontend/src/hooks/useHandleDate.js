import dayjs from 'dayjs';
import isYesterday from 'dayjs/plugin/isYesterday';

const useFormatDate = (dateStr) => {
  dayjs.extend(isYesterday);
  const date = dayjs(dateStr);
  const today = dayjs();

  if (today.format('DD/MM/YY') === date.format('DD/MM/YY')) {
    return date.format('h:mm A');
  } else if (date.isYesterday()) {
    return 'Yesterday';
  }
  return `${date.format('MMM')} ${date.format('DD')}`;
};

export default useFormatDate;
