import useFormatDate from './useHandleDate';

const successCode = 200;

const getNews = async () => {
  const response = await fetch(`${process.env.REACT_APP_BACKEND_URL}/news`);
  const data = await response.json();
  if (response.status !== successCode) {
    console.log(data.error);
    return [];
  }
  return data;
};

export const useHandleGetNews = async () => {
  const handleFormatDate = useFormatDate;
  const news = await getNews();

  const formatedNews = news
    .sort((x, y) => new Date(y.created_at) - new Date(x.created_at))
    .map((element) => ({
      ...element,
      created_at: handleFormatDate(element.created_at),
    }));

  return formatedNews;
};

export const useHandleDeleteNew = async (objectID) => {
  const response = await fetch(
    `${process.env.REACT_APP_BACKEND_URL}/news/${objectID}`,
    {
      method: 'DELETE',
    },
  );
  return response.status;
};
